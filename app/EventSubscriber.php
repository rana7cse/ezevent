<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventSubscriber extends Model
{
    //
    protected $table = "event_subscriber";
    protected $fillable = [
        'user_id',
        'ticket_id',
        'event_id'
    ];
}
