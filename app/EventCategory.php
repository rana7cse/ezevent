<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventCategory extends Model
{
    //
    protected $table = "event_category";

    protected $fillable = [
        'name',
        'slug',
        'parent_id'
    ];
}
