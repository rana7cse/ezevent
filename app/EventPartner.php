<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventPartner extends Model
{
    //
    protected $table = 'event_partner';
    protected $fillable = [
        'name',
        'profile',
        'city',
        'country',
        'logo',
        'contact',
        'email',
        'url',
        'event_id'
    ];

    public function event(){
        return $this->belongsTo('App\Event','event_id');
    }
}
