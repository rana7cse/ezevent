<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model
{
    use SoftDeletes;
    //
    protected $table = 'events';

    protected $fillable = [
        'name',
        'details',
        'start_time',
        'end_time',
        'cover_img',
        'street',
        'state',
        'zip_code',
        'country',
        'event_category',
        'user_id',
    ];

    public function user(){
        return $this->belongsTo('App\User','user_id');
    }

    public function partners(){
        return $this->hasMany('App\EventPartner','event_id');
    }

    public function speakers(){
        return $this->hasMany('App\EventSpeakers','event_id');
    }

    public function tickets(){
        return $this->hasMany('App\EventTicket','event_id');
    }
}
