<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventRole extends Model
{
    //
    protected $table = "event_role";

    protected $fillable = [
        'user_id',
        'event_id'
    ];
}
