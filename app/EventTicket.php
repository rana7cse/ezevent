<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventTicket extends Model
{
    //
    protected $table = "event_tickets";
    protected $fillable = [
        'name',
        'details',
        'price',
        'currency',
        'event_id'
    ];

    public function event(){
        return $this->belongsTo('App\Event','event_id');
    }
}
