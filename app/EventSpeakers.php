<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventSpeakers extends Model
{
    //
    protected $table = "event_speakers";

    protected $fillable = [
        'full_name',
        'bio',
        'job_title',
        'company',
        'city',
        'country',
        'photo',
        'contact',
        'email',
        'url',
        'event_id'
    ];

    public function event(){
        return $this->belongsTo('App\Event','event_id');
    }
}
