<?php

namespace App\Http\Controllers\User;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        //View profile page;
        return view('profile.index');
    }

    public function updateImage(Request $request){
        if($request->hasFile('my_image')){
            $id = Auth::user()->id;
            $user = User::find($id);

            $photo = $request->file('my_image');
            $name = uniqid().".".$photo->getClientOriginalExtension();
            $photo->move("image/user/",$name);
            $user->img_url = $name;
            $user->save();
            return ["image" => url("image/user/{$name}")];
        }
    }

    public function profileUpdate(Request $request){
        $data = $request->except(['img_url','id','activation_code','deleted_at','created_at','updated_at']);
        $user = User::find(Auth::user()->id);
        if($user->update($data)){
            return response()->json(['code'=>200,'massage' => 'Successfully user information updated!'],200);
        }
        return response()->json(['code' => 400,'massage' => 'Problem in profile updating'],500);
    }

    public function getUserInfo(){
        $user = User::find(Auth::user()->id);
        $user->img_url = (isset($user->img_url) && $user->img_url=="") ? "https://s3.amazonaws.com/100minds/people/medium/missing.png" : url("image/user/{$user->img_url}");
        $user->setHidden(['id','remember_token','activation_code','deleted_at','created_at','updated_at','password','user_role']);
        return $user;
    }
}
