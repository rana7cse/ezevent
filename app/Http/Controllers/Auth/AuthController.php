<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\Request;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;
    

    public function index(){
        return view('auth.index');
    }

    public function doLogin(LoginRequest $loginRequest){
        $data = [];
        if(Auth::attempt(['email' => $loginRequest->input('email'),'password' => $loginRequest->input('password')])){
            $data['url'] = Session::get('url.intended', url('/'));
            $data['code'] = 200;
            $data['massage'] = "Successfully logged in";
            return response()->json($data,200);
        }

        $data['code'] = 404;
        $data['massage'] = "Email and password is not valid";
        return response()->json($data,200);
    }

    public function doLogout(){
        if(Auth::check()){
            Auth::logout();
            return redirect('/');
        }
        return redirect('/');
    }
}
