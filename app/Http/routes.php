<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
/*
 * Site Home Page
 * */
Route::get('/', 'HomeController@index');

/*
 * Site Login page
 * */
Route::group(['middleware' => ['web']],function(){
   Route::group(['middleware' => ['auth']],function(){
      //------ user profile controller -------->
      Route::get('/profile','User\UserController@index');
      Route::post('/profile/update','User\UserController@profileUpdate');
      Route::post('/profile/image/update','User\UserController@updateImage');
      Route::get('/profile/show','User\UserController@getUserInfo');
      
      //------------- Event controller ------------
      Route::get('/event/create','EventController@create');
      
   });
   // ------- Login controller ------------>
   Route::get('/login','Auth\AuthController@index');
   Route::post('/login','Auth\AuthController@doLogin');
   Route::get('/logout','Auth\AuthController@doLogout');
});
