<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventRoleType extends Model
{
    //
    protected $table = "event_role_type";

    protected $fillable = [
        'name',
        'slug'
    ];
}
