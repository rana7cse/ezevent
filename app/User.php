<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = "users";

    protected $fillable = [
        'first_name',
        'last_name',
        'street',
        'state',
        'zip_code',
        'country',
        'contact',
        'gender',
        'date_of_birth',
        'activation_code',
        'email',
        'password',
        'user_role',
        'img_url'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function event(){
        return $this->hasMany('App\Event','user_id');
    }
}
