<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventSpeakers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_speakers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('full_name');
            $table->text('bio');
            $table->string('job_title');
            $table->string('company');
            $table->string('city');
            $table->string('country');
            $table->string('photo');
            $table->string('contact');
            $table->string('email');
            $table->string('url');
            $table->integer('event_id')->unsigned();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('event_speakers');
    }
}
