<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        $this->call(eventCategorySeeder::class);
        $this->call(eventRoleSeeder::class);
        $this->call(userGenderTableSeeder::class);
        $this->call(userRoleTableSeeder::class);
    }
}
