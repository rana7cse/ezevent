<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class eventRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\EventRoleType::insert([
            [
                'name' => 'Admin',
                'slug' => 'admin'
            ],
            [
                'name' => 'Owner',
                'slug' => 'owner'
            ],
            [
                'name' => 'Volunteer',
                'slug' => 'volunteer'
            ]
        ]);
    }
}
