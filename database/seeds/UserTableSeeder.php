<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'first_name' => 'Mokkel',
            'last_name' => 'Khan',
            'street' => 'Choko Street',
            'state' => 'Dhaka',
            'zip_code' => 1216,
            'country' => 'Bangladesh',
            'contact' => '01724058171',
            'gender' => 'male',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('123456'),
            'user_role' => 'user'
        ]);
    }
}
