<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class userGenderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users_gender')->insert([
           [
               'name' => 'Male',
               'slug' => 'male'
           ],
            [
                'name' => 'Female',
                'slug' => 'female'
            ],
        ]);
    }
}
