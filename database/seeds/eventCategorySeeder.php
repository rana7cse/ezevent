<?php

use Illuminate\Database\Seeder;

class eventCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        \App\EventCategory::insert([
            [
                'name' => 'Business',
                'slug' => 'business',
                'parent_id' => 0
            ],
            [
                'name' => 'Science & Tech',
                'slug' => 'science_&_tech',
                'parent_id' => 0
            ],
            [
                'name' => 'Music',
                'slug' => 'music',
                'parent_id' => 0
            ],
            [
                'name' => 'charity & causes',
                'slug' => 'charity_&_causes',
                'parent_id' => 0
            ],
            [
                'name' => 'Community',
                'slug' => 'community',
                'parent_id' => 0
            ]
        ]);
    }
}
