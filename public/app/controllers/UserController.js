app.controller('UserController',function ($http,ajaxService,CSRF_TOKEN,ngToast){
    var thisUser = this;
    thisUser.data = {};

    thisUser.showProfileInfo = function () {
        $http.get(rootUrl+"profile/show")
            .then(function (e) {
                thisUser.data = e.data;
            },function (e) {
                console.log(e);
            });
    };
    thisUser.showProfileInfo();

    thisUser.updateProfileInfo = function (valid) {
        if(valid){
            $http.post(rootUrl+"profile/update",thisUser.data)
                .then(function (e) {
                    thisUser.showProfileInfo();
                },function (e) {
                   console.log(e);
                });
        }
    };

    /*
    * Page util
    * */
    thisUser.formatDob = function (nd,od) {
        thisUser.data.date_of_birth = moment(nd).format("YYYY-MM-DD");
    }
});