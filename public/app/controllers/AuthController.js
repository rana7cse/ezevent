app.controller('AuthController',function ($http,ajaxService,CSRF_TOKEN,ngToast){
    var thisAuth = this;
    this.email = "";
    this.password = "";
    
    this.doLogin = function (isvalid) {
        if(isvalid){
            var data = {
                'email' : thisAuth.email,
                'password' : thisAuth.password,
                '_token' : CSRF_TOKEN
            };
            ajaxService.post("login",data)
                .then(function(res){
                    if(res.data.code == 200){
                        window.location = res.data.url
                    } else {
                        ngToast.create({
                            "className" : "danger",
                            "content" : res.data.massage
                        })
                    }
                },function(err){
                    console.log(err);
                });
        }
    };
});