var rootUrl = $('#root_url').val()+"/";

var app = angular
    .module("ezEvent",['ngToast','ui.bootstrap.datetimepicker'])
    .constant("CSRF_TOKEN", $('input[name=_token]').val());

/*
* | Factory_______ Service
* |
* */

app.service('ajaxService',function ($http) {
    this.get = function (url) {
        return $http.get(rootUrl+url);
    };

    this.post = function (url,data) {
        return $http({
            method: 'POST',
            url: rootUrl+url,
            headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
            data: $.param(data)
        });
    };
});

/*
* Directive
* */
