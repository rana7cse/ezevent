@extends('common.layout')
@section('section-body')
    <div class="home-banner" id="slider_banner" style="padding: 50px 0;" ng-controller="AuthController as user">
        <div class="overlay"></div>
            <div class="login_boxo">
                <form class="modal-content" name="loginForm" ng-submit="user.doLogin(loginForm.$valid)" novalidate>
                    <div class="modal-header">
                        <div class="modal_big_header">
                            <div class="icon">
                                <i class="fa fa-user"></i>
                            </div>
                            <div class="heading">
                                Sign In to EZevent
                            </div>
                            <div class="sub_heading">
                                Let’s get started.
                            </div>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">Email</label>
                            <input type="email" required name="email" class="form-control" ng-model="user.email" placeholder="Your email address">
                            <div class="form_error_ez" ng-show="loginForm.email.$error && loginForm.email.$touched">
                                <p ng-show="loginForm.email.$error.email">Your email id is not valid</p>
                                <p ng-show="loginForm.email.$error.required">Your email id is required.</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">Password</label>
                            <input type="password" ng-minlength="6" ng-maxlength="14" required name="password" ng-model="user.password" class="form-control" placeholder="Your password">

                            <div class="form_error_ez" ng-show="loginForm.password.$error && loginForm.password.$touched">
                                <p ng-show="loginForm.password.$error.minlength">Your password is too short.</p>
                                <p ng-show="loginForm.password.$error.maxlength">Your password is too long.</p>
                                <p ng-show="loginForm.password.$error.required">Your password is required.</p>
                            </div>
                        </div>
                        {{csrf_field()}}
                        <div class="sb_btn">
                            <button class="btn btn-success btn-lg btn-block" type="submit" ng-disabled="loginForm.$invalid">Sign in</button>
                        </div>
                        <div class="clearfix"></div>
                        <p class="new_user_control">
                            <a href="#" class="body-1-dark">Sign Up Now</a>
                            <span class="caption-dark"> • </span>
                            <a href="#" class="body-1-dark">Forgot Password?</a></p>
                    </div>
                </form>
        </div>
    </div>
@endsection

@section('footer-script')
    {!! Html::script('app/controllers/AuthController.js') !!}
@endsection