@extends('common.layout')
@section('section-body')
    <div class="main_page_content_box event_page" style="padding: 0" ng-controller="EventController">
        <div class="page_header_box">
            <div class="container">
                <div class="title">
                    <div class="ico pull-left">
                        <i class="fa fa-calendar-o"></i>
                    </div>
                    <h2 class="pull-left"> Create an event</h2>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="page_content">
                <div class="create_event_form">
                    <form name="createEventForm" class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="firstName">Event title</label>
                                <input type="text" class="form-control" name="firstName" id="firstName"
                                       placeholder="Type your event title here" required="">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer-script')
    {!! Html::script('app/controllers/EventController.js') !!}
@endsection