<div class="nav pull-right">
    <ul class="list-inline">
        <li><a href="#">Browse Event</a></li>

        @if(Auth::user())
        <li class="dropdown">
            <a href="#" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true">
                <i class="fa fa-user" style="margin-right: 5px;"></i>
                {{(Auth::user()->first_name)? Auth::user()->first_name : "Your name"}} {{Auth::user()->last_name}}
                <span class="caret" style="margin-top: -5px;"></span>
            </a>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                <li><a href="#"><i class="fa fa-coffee" aria-hidden="true"></i> My Event</a></li>
                <li><a href="#"><i class="fa fa-hdd-o"></i> Save Events</a></li>
                <li><a href="#"><i class="fa fa-plug"></i> Manage Events</a></li>
                <li><a href="{{url("/profile")}}"><i class="fa fa-user"></i> My Profile</a></li>
                <li><a href="{{url('/logout')}}"><i class="fa fa-sign-out"></i> Logout</a></li>
            </ul>
        </li>
        @else
        <li><a href="{{url('/login')}}">Sign in</a></li>
        <li><a href="{{url('/login')}}">Sign up</a></li>
        @endif

        <li class="ext"><a href="{{url("/event/create")}}">Create Event</a></li>
    </ul>
</div>