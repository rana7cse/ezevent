<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>EzEventS</title>

    <!--Dependencies Css-->
    {!! Html::style('layout/bower_components/bootstrap-css/css/bootstrap.min.css') !!}
    {!! Html::style('bower_components/angular-bootstrap-datetimepicker/src/css/datetimepicker.css') !!}
    {!! Html::style('bower_components/ngToast/dist/ngToast.min.css') !!}

    <!--Custom Styles-->
    {!! Html::style('layout/css/style.css') !!}

    <!--Google fonts-->
    {!! Html::style('https://fonts.googleapis.com/css?family=Averia+Serif+Libre') !!}
    {!! Html::style('https://fonts.googleapis.com/css?family=Lato') !!}

    <!--Font awesome-->
    {!! Html::style('https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css') !!}

</head>
<body ng-app="ezEvent">

<div class="page-wrapper">

    @include('common.header')

    @yield('section-body')

    @include('common.footer')

</div>

{!! Html::script('layout/bower_components/jquery/jquery.js') !!}
{!! Html::script('layout/bower_components/bootstrap-css/js/bootstrap.min.js') !!}
{!! Html::script('layout/bower_components/jquery.backstretch.min/index.js') !!}
{!! Html::script("bower_components/moment/min/moment.min.js") !!}
{!! Html::script('layout/js/script.js') !!}
{!! Html::script('layout/bower_components/angular/angular.min.js') !!}
{!! Html::script('bower_components/angular-animate/angular-animate.min.js') !!}
{!! Html::script('bower_components/angular-sanitize/angular-sanitize.min.js') !!}
{!! Html::script('bower_components/angular-bootstrap-datetimepicker/src/js/datetimepicker.js') !!}
{!! Html::script('bower_components/angular-bootstrap-datetimepicker/src/js/datetimepicker.templates.js') !!}
{!! Html::script('bower_components/ngToast/dist/ngToast.min.js') !!}
{!! Html::script('app/app.js') !!}

@yield('footer-script')

</body>
</html>