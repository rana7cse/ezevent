@extends('common.layout')
@section('section-body')
    <div class="main_page_content_box" ng-controller="UserController as user">
        <div class="container">
            <div class="page_header_box">
                <div class="title">
                    <div class="ico pull-left">
                        <i class="fa fa-user"></i>
                    </div>
                    <h2 class="pull-left"> User profile</h2>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="page_content">
                <div class="row">
                    <div class="col-md-3 profile_image">
                        <div class="img_box">
                            <img id="profile_priview_image" ng-src="@{{ user.data.img_url }}" alt="">
                        </div>
                        <div class="form-group" style="margin-top: 10px;">
                            <label for="changeProfile" class="btn btn-block btn-default">Change photo</label>
                            <input type="file" class="form-control invisible-input" id="changeProfile" placeholder="Email">
                        </div>
                    </div>
                    <div class="col-md-9">
                        <form class="row" ng-submit="user.updateProfileInfo(profileInfo.$valid)" name="profileInfo" novalidate>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="firstName">First name</label>
                                    <input type="text" class="form-control" name="firstName" id="firstName"
                                           ng-model="user.data.first_name" placeholder="First name" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Last Name</label>
                                    <input type="text" class="form-control" name="lastName" id="exampleInputEmail1"
                                           ng-model="user.data.last_name" placeholder="Last name" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Address</label>
                                    <input type="text" class="form-control" name="addrStreet" id="exampleInputEmail1"
                                           placeholder="Street address" ng-model="user.data.street">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">City</label>
                                    <input type="text" class="form-control" name="addrCity" id="exampleInputEmail1"
                                           placeholder="City" ng-model="user.data.state">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Zip code</label>
                                    <input type="text" class="form-control" name="addrZip" id="exampleInputEmail1"
                                           placeholder="Zip Code" ng-model="user.data.zip_code">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Country</label>
                                    <input type="text" class="form-control" name="addrCountry" id="exampleInputEmail1"
                                           placeholder="Country name" ng-model="user.data.country">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Gender</label>
                                    <select class="form-control" name="userGender" id="userGender" ng-model="user.data.gender">
                                        <option value="">Select Gender</option>
                                        <option value="male">Male</option>
                                        <option value="female">Female</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Date of birth</label>
                                    <a class="dropdown-toggle" id="dLabel" role="button" data-toggle="dropdown" data-target="#" href="">
                                        <input type="text" name="userDateOfBirth" ng-model="user.data.date_of_birth"
                                           class="form-control" id="exampleInputEmail1" placeholder="DD-MM-YYYY">
                                    </a>
                                    <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                                        <datetimepicker data-ng-model="user.data.date_of_birth"
                                                        data-datetimepicker-config="{ dropdownSelector: '.dropdown-toggle','minView' : 'day','startView' :'year'}"
                                                        data-on-set-time="user.formatDob(newDate,oldDate)"></datetimepicker>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Mobile No</label>
                                    <input type="text" name="userMobileNo" class="form-control" id="exampleInputEmail1"
                                           ng-model="user.data.contact" placeholder="XXXXXXXXXXXX">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">About me</label>
                                    <textarea class="form-control" name="userAboutHim" id="exampleInputEmail1"
                                              placeholder="Write something about yourself" ng-model="user.data.about_me"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group pull-right">
                                    <button class="btn btn-success" type="submit">Update profile</button>
                                </div>
                            </div>
                            {{csrf_field()}}
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer-script')
    {!! Html::script('app/controllers/UserController.js') !!}

    <script>
        $(function() {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('input[name=_token]').val()
                },
                processData: false
            });

            $('#changeProfile').change(function () {
                var myData = new FormData();
                myData.append('my_image',$(this)[0].files[0]);
                myData.append('_token',$('input[name=_token]').val());
                //-- This is my work man --
                var ajAx = new XMLHttpRequest();
                ajAx.open("POST",rootUrl+'profile/image/update');
                ajAx.send(myData);

                ajAx.onreadystatechange = function(){
                    if(ajAx.readyState == 4 && ajAx.status == 200){
                        try{
                            var resp = ajAx.response;
                        } catch (e){
                            var resp = {
                                "status" : "error",
                                "error" : ajAx.responseText
                            }
                        }
                        if(resp){
                            console.log(resp);
                        }
                    }
                };

                var fileRead = new FileReader();
                fileRead.onload = function(e){
                    $('#profile_priview_image').attr('src',e.target.result)

                };
                fileRead.readAsDataURL($(this)[0].files[0]);
            });
        });
    </script>
@endsection